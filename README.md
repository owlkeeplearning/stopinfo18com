# stopinfo18com



## Getting started

As usual I receive an SMS, saying I have a pending parcel and ask me to open a link:

https://rebooking.info-18.com/

## Try on browser

The link can be opened on Android.

If open the link in Windows, it shows 404.

## Break the site

By little tricks of adding user agent and cookies with CURL, the followings were found:

* The target identifies HOST header, -H "Host:rebooking.info-18.com"
* The landing page Javascript is encoded, I use an online Javascript executor https://onecompiler.com/javascript/ to run the script by printing to console.log instead of document.write.
* The target uses Javascript to detect the browser is a BOT or not, if yes, then redirect to 404 page/show.php.
* The target identifies the session id and must be sent in cookies in all subsequence requests.
* The target identifies additional cookies values: x10=ok, path=/
* The target URL arguments, id and session, are the same for all requests.
* It includes credit card validation library in the credit card input page, but can be by-passed by sending the requests to target directly.
* The final submit form action looks broken. I made it work in command line.
* After sending credit card information. it redirects to https://www.evri.com/ using location redirect html header, so you don't see the complete page content.
* It comes to the end when all responses return header, content-length: 0, seems the target is unable to handle any requests anymore.
